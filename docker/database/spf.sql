-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.49.2:3306
-- Erstellungszeit: 28. Nov 2022 um 14:58
-- Server-Version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP-Version: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Datenbank: `spf`
--
USE `spf`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `asns`
--

CREATE TABLE `asns` (
  `domain` varchar(256) NOT NULL,
  `asn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `domains`
--

CREATE TABLE `domains` (
  `domain` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `empty`
--

CREATE TABLE `empty` (
  `domain` varchar(256) NOT NULL,
  `has_mx` tinyint(1) NOT NULL,
  `dnssec` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `errors`
--

CREATE TABLE `errors` (
  `domain` varchar(256) NOT NULL,
  `mechanism` varchar(256) DEFAULT NULL,
  `type` enum('error','warning') DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `msg` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `includes`
--

CREATE TABLE `includes` (
  `include` varchar(256) NOT NULL,
  `domain` varchar(256) NOT NULL,
  `qualifier` enum('pass','neutral','softfail','fail') DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `include_asns`
--

CREATE TABLE `include_asns` (
  `domain` varchar(256) NOT NULL,
  `asn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `include_errors`
--

CREATE TABLE `include_errors` (
  `domain` varchar(256) NOT NULL,
  `mechanism` varchar(256) DEFAULT NULL,
  `type` enum('error','warning') NOT NULL,
  `class` varchar(40) NOT NULL,
  `msg` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `include_mechanisms`
--

CREATE TABLE `include_mechanisms` (
  `domain` varchar(256) NOT NULL,
  `mechanism` varchar(32) NOT NULL,
  `value` varchar(256) DEFAULT NULL,
  `qualifier` enum('pass','neutral','softfail','fail') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `include_results`
--

CREATE TABLE `include_results` (
  `domain` varchar(256) NOT NULL,
  `record` varchar(500) NOT NULL,
  `dns_lookups` int(11) NOT NULL,
  `dns_void_lookups` int(11) NOT NULL,
  `allowed_ip4` int(11) NOT NULL,
  `allowed_ip6` binary(32) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mechanisms`
--

CREATE TABLE `mechanisms` (
  `domain` varchar(256) NOT NULL,
  `mechanism` varchar(32) NOT NULL,
  `value` varchar(256) DEFAULT NULL,
  `qualifier` enum('pass','neutral','softfail','fail') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spf`
--

CREATE TABLE `spf` (
  `domain` varchar(256) NOT NULL,
  `has_mx` tinyint(1) NOT NULL,
  `spf` varchar(500) NOT NULL,
  `valid` tinyint(1) NOT NULL,
  `incomplete` tinyint(1) NOT NULL,
  `n_lookups` int(11) NOT NULL,
  `n_void_lookups` int(11) NOT NULL,
  `n_errors` int(11) NOT NULL,
  `n_includes` int(11) NOT NULL,
  `n_includes_recursive` int(11) NOT NULL,
  `n_ip4` int(11) NOT NULL,
  `n_ip6` binary(32) NOT NULL,
  `n_asn` int(11) NOT NULL,
  `n_asn_complete` tinyint(1) NOT NULL,
  `dmarc` varchar(256) DEFAULT NULL,
  `dmarc_valid` tinyint(1) DEFAULT NULL,
  `dmarc_p` enum('none','quarantine','reject') DEFAULT NULL,
  `dmarc_sp` enum('none','quarantine','reject') DEFAULT NULL,
  `dmarc_adkim` enum('r','s') DEFAULT NULL,
  `dmarc_aspf` enum('r','s') DEFAULT NULL,
  `dnssec` tinyint(1) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 `PAGE_COMPRESSED`='ON';

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `asns`
--
ALTER TABLE `asns`
  ADD KEY `FK_asns` (`domain`);

--
-- Indizes für die Tabelle `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`domain`);

--
-- Indizes für die Tabelle `empty`
--
ALTER TABLE `empty`
  ADD PRIMARY KEY (`domain`);

--
-- Indizes für die Tabelle `errors`
--
ALTER TABLE `errors`
  ADD KEY `FK_errors` (`domain`);

--
-- Indizes für die Tabelle `includes`
--
ALTER TABLE `includes`
  ADD KEY `FK_includes` (`domain`);

--
-- Indizes für die Tabelle `include_asns`
--
ALTER TABLE `include_asns`
  ADD KEY `FK_include_asns` (`domain`);

--
-- Indizes für die Tabelle `include_errors`
--
ALTER TABLE `include_errors`
  ADD KEY `FK_include_errors` (`domain`);

--
-- Indizes für die Tabelle `include_mechanisms`
--
ALTER TABLE `include_mechanisms`
  ADD KEY `FK_include_mechanisms` (`domain`);

--
-- Indizes für die Tabelle `include_results`
--
ALTER TABLE `include_results`
  ADD PRIMARY KEY (`domain`);

--
-- Indizes für die Tabelle `mechanisms`
--
ALTER TABLE `mechanisms`
  ADD KEY `FK_mechanisms` (`domain`);

--
-- Indizes für die Tabelle `spf`
--
ALTER TABLE `spf`
  ADD PRIMARY KEY (`domain`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `asns`
--
ALTER TABLE `asns`
  ADD CONSTRAINT `FK_asns` FOREIGN KEY (`domain`) REFERENCES `spf` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `errors`
--
ALTER TABLE `errors`
  ADD CONSTRAINT `FK_errors` FOREIGN KEY (`domain`) REFERENCES `spf` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `include_asns`
--
ALTER TABLE `include_asns`
  ADD CONSTRAINT `FK_include_asns` FOREIGN KEY (`domain`) REFERENCES `include_results` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `include_errors`
--
ALTER TABLE `include_errors`
  ADD CONSTRAINT `FK_include_errors` FOREIGN KEY (`domain`) REFERENCES `include_results` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `include_mechanisms`
--
ALTER TABLE `include_mechanisms`
  ADD CONSTRAINT `FK_include_mechanisms` FOREIGN KEY (`domain`) REFERENCES `include_results` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `mechanisms`
--
ALTER TABLE `mechanisms`
  ADD CONSTRAINT `FK_mechanisms` FOREIGN KEY (`domain`) REFERENCES `spf` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE;

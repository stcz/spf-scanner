from collections import OrderedDict

import pandas as pd
from sqlalchemy import create_engine, text
from sqlalchemy.exc import SQLAlchemyError


# Import Errors for eval() to get errors from DB
from .checkdmarc import SMTPError, SPFError, _NotImplemented, _SPFWarning, _SPFMissingRecords, _SPFUnusedMechanism, \
    _SPFWrongNetworkAddress, _SPFDuplicateInclude, _SPFDeprecatedPTR, _SPFDeprecatedSPFRecord, _DMARCWarning, \
    _BIMIWarning, _DMARCBestPracticeWarning, _DMARCUnusedRecordWarning, DNSException, DNSExceptionNXDOMAIN, DMARCError, \
    SPFRecordNotFound, MultipleSPFRTXTRecords, SPFSyntaxError, SPFUnknownMechanism, SPFTooManyDNSLookups, \
    SPFTooManyVoidDNSLookups, SPFRedirectLoop, SPFIncludeLoop, SPFInvalidIP, DMARCRecordNotFound, DMARCSyntaxError, \
    InvalidDMARCTag, InvalidDMARCTagValue, InvalidDMARCReportURI, UnrelatedTXTRecordFoundAtDMARC, \
    SPFRecordFoundWhereDMARCRecordShouldBe, DMARCRecordInWrongLocation, DMARCReportEmailAddressMissingMXRecords, \
    UnverifiedDMARCURIDestination, MultipleDMARCRecords, BIMIError, BIMIRecordNotFound, BIMISyntaxError, InvalidBIMITag, \
    InvalidBIMITagValue, InvalidBIMIIndicatorURI, UnrelatedTXTRecordFoundAtBIMI, SPFRecordFoundWhereBIMIRecordShouldBe, \
    BIMIRecordInWrongLocation, MultipleBIMIRecords, _SPFGrammar, _DMARCGrammar, _BIMIGrammar


class SPFDatabaseWorker:
    # Variables
    DATABASE_INSERT_CHUNK_SIZE = 10000

    def __init__(self, database_engine):
        self.worker = create_engine(database_engine)

    def get_table(self, table, selector=None):
        return pd.read_sql_query(f"SELECT {selector if selector else '*'} FROM {table}", self.worker)

    def get_data(self, table: str, selector: str = '*', conditions=None, condition_operator="AND"):
        condition_str = ""
        if conditions:
            if isinstance(conditions, list):
                condition_str = f" {condition_operator} ".join(conditions)
            elif isinstance(conditions, str):
                condition_str = conditions
        return pd.read_sql_query(
            f"SELECT {selector} FROM {table}{(' WHERE ' + condition_str) if len(condition_str) > 0 else ''}",
            self.worker)

    def get_count(self, table: str, column: str, conditions=None, condition_operator="AND", distinct=True) -> int:
        condition_str = ""
        if conditions:
            if isinstance(conditions, list):
                condition_str = f" {condition_operator} ".join(conditions)
            elif isinstance(conditions, str):
                condition_str = conditions
        query = text(
            f"SELECT COUNT({'DISTINCT(' if distinct else ''}`{column}`{')' if distinct else ''}) FROM {table}{(' WHERE ' + condition_str) if len(condition_str) > 0 else ''}")
        result = self.worker.execute(query)
        return result.fetchone()[0]

    def get_include_errors_obj(self, domain, type="error"):
        query = text(
            "SELECT mechanism, class, msg FROM include_errors WHERE `include_errors`.`domain` = :domain AND  `include_errors`.`type` = :type")
        include_errors = self.worker.execute(query, domain=domain, type=type).fetchall()
        errors = []
        for error in include_errors:
            obj = eval(error[1])(error[2], mechanism=error[0])
            errors.append(obj)
        return errors

    def get_include_qualifier(self, domain, include):
        query = text(
            "SELECT qualifier FROM includes WHERE `includes`.`domain` = :domain AND `includes`.`include` = :include")
        result = self.worker.execute(query, domain=domain, include=include).fetchone()
        if not result:
            return None
        return result[0]

    def get_include_asns(self, domain):
        query = text("SELECT asn FROM include_asns WHERE `include_asns`.`domain` = :domain")
        include_asns = self.worker.execute(query, domain=domain).fetchall()
        return [item[0] for item in include_asns]

    def get_include_results_ojb(self, domain, root_domain):
        query = text("SELECT * FROM include_results WHERE `include_results`.`domain` = :domain")
        include_result = self.worker.execute(query, domain=domain).fetchone()
        if not include_result:
            return None
        parsed = self._get_parsed_includes(domain)
        return OrderedDict(
            [('domain', domain), ('record', include_result[1]), ('dns_lookups', include_result[2]),
             ('dns_void_lookups', include_result[3]),
             ('allowed_ip4', include_result[4]),
             ('allowed_ip6', int.from_bytes(include_result[5], byteorder='big', signed=False)),
             ('asns', self.get_include_asns(domain)), ("parsed", parsed),
             ("qualifier", self.get_include_qualifier(root_domain, domain)),
             ("warnings", self.get_include_errors_obj(domain, "warning")),
             ("errors", self.get_include_errors_obj(domain, "error"))])

    def _get_parsed_includes(self, domain):
        query = text(
            "SELECT mechanism, value, qualifier FROM include_mechanisms WHERE `include_mechanisms`.`domain` = :domain")
        include_mechanisms = self.worker.execute(query, domain=domain).fetchall()
        parsed = {
            "pass": [],
            "neutral": [],
            "softfail": [],
            "fail": [],
            "include": [],
            "redirect": None,
            "exp": None,
            "all": None,
        }
        for mechanism, value, qualifier in include_mechanisms:
            if mechanism in ("include"):
                parsed[mechanism].append(self.get_include_results_ojb(value, domain))
            elif mechanism in ("redirect"):
                parsed[mechanism] = self.get_include_results_ojb(value, domain)
            elif mechanism in ("exp"):
                parsed[mechanism] = value
            elif mechanism in ("all"):
                parsed[mechanism] = qualifier
            else:
                parsed[qualifier].append(OrderedDict([("value", value), ("mechanism", mechanism)]))
        return parsed

    def delete_domain(self, domain):
        try:
            query = text("DELETE FROM spf WHERE `spf`.`domain` = ':domain' ")
            self.worker.execute(query, domain=domain)
            query = text("DELETE FROM empty WHERE `empty`.`domain` = ':domain' ")
            self.worker.execute(query, domain=domain)
        except SQLAlchemyError as e:
            print(f"Database Error: '{str(e)}'")

    def _get_or_none(self, object, value):
        try:
            return object.__getattribute__(value)
        except:
            return None

    def insert_spf(self, domain, has_mx, spf, valid, incomplete, n_errors, n_includes, n_includes_recursive, n_ip4,
                   n_ip6, n_asn, n_asn_complete, dmarc, dmarc_valid, dmarc_p, dmarc_sp, dmarc_adkim, dmarc_aspf,
                   dnssec):
        try:
            query = text(
                "INSERT IGNORE INTO `spf`(`domain`, `has_mx`, `spf`, `valid`, `incomplete`, `n_errors`, `n_includes`, `n_includes_recursive`, `n_ip4`, `n_ip6`, `n_asn`, `n_asn_complete`, `dmarc`, `dmarc_valid`, `dmarc_p`, `dmarc_sp`, `dmarc_adkim`, `dmarc_aspf`, `dnssec`) "
                "VALUES (:domain, :has_mx, :spf, :valid, :incomplete, :n_errors, :n_includes, :n_includes_recursive, :n_ip4, :n_ip6, :n_asn, :n_asn_complete, :dmarc, :dmarc_valid, :dmarc_p, :dmarc_sp, :dmarc_adkim, :dmarc_aspf, :dnssec)")
            self.worker.execute(query, domain=domain, has_mx=has_mx, spf=spf, valid=valid, incomplete=incomplete,
                                n_errors=n_errors, n_includes=n_includes, n_includes_recursive=n_includes_recursive,
                                n_ip4=n_ip4,
                                n_ip6=int.to_bytes(n_ip6, length=32, byteorder='big', signed=False) if n_ip6 else None,
                                n_asn=n_asn, n_asn_complete=n_asn_complete,
                                dmarc=dmarc, dmarc_valid=dmarc_valid, dmarc_p=dmarc_p, dmarc_sp=dmarc_sp,
                                dmarc_adkim=dmarc_adkim, dmarc_aspf=dmarc_aspf,
                                dnssec=dnssec)
        except SQLAlchemyError as e:
            print(f"Database Error: '{str(e)}'")

    def insert_includes(self, domain, includes):
        try:
            if len(includes) == 0:
                return
            kwargs = {}
            list = []
            for i, d in enumerate(includes):
                kwargs[f"include{i}"] = d.get('domain')
                kwargs[f"domain{i}"] = domain
                kwargs[f"qualifier{i}"] = d.get('qualifier')
                list.append(f":include{i}, :domain{i}, :qualifier{i}")
                if len(list) >= self.DATABASE_INSERT_CHUNK_SIZE:
                    query = text("INSERT IGNORE INTO `includes`(`include`, `domain`, `qualifier`) VALUES (" +
                                 "), (".join(list) +
                                 ")")
                    self.worker.execute(query, kwargs)
                    list = []
            if len(list) > 0:
                query = text("INSERT IGNORE INTO `includes`(`include`, `domain`, `qualifier`) VALUES (" +
                             "), (".join(list) +
                             ")")
                self.worker.execute(query, kwargs)
        except SQLAlchemyError as e:
            print(f"Database Error: '{str(e)}'")

    def insert_include_result(self, include_result):
        try:
            query = text(
                "INSERT IGNORE INTO `include_results`(`domain`, `record`, `dns_lookups`, `dns_void_lookups`, `allowed_ip4`, `allowed_ip6`) "
                "VALUES (:domain, :record, :dns_lookups, :dns_void_lookups, :allowed_ip4, :allowed_ip6)")
            self.worker.execute(query, domain=include_result["domain"], record=include_result["record"],
                                dns_lookups=include_result["dns_lookups"],
                                dns_void_lookups=include_result["dns_void_lookups"],
                                allowed_ip4=include_result['allowed_ip4'],
                                allowed_ip6=int.to_bytes(include_result['allowed_ip6'], length=32, byteorder='big',
                                                         signed=False) if include_result['allowed_ip6'] else None)
            self.insert_errors(include_result["domain"], include_result["errors"], 'error', table="include_errors")
            self.insert_errors(include_result["domain"], include_result["warnings"], 'warning', table="include_errors")
            self.insert_asns(include_result["domain"], include_result["asns"], table="include_asns")
            self.insert_mechanisms(include_result["domain"], include_result["parsed"], table="include_mechanisms")
            self.insert_includes(include_result["domain"], include_result["parsed"]["include"])
        except SQLAlchemyError as e:
            print(f"Database Error: '{str(e)}'")

    def insert_mechanisms(self, domain, mechanisms, table="mechanisms"):
        try:
            if len(mechanisms) == 0:
                return
            kwargs = {}
            list = []
            for i, key in enumerate(mechanisms.keys()):
                if key in ("pass", "neutral", "softfail", "fail"):
                    for j, m in enumerate(mechanisms[key]):
                        kwargs[f"domain{i}_{j}"] = domain
                        kwargs[f"mechanism{i}_{j}"] = m.get('mechanism')
                        kwargs[f"value{i}_{j}"] = m.get('value')
                        kwargs[f"qualifier{i}_{j}"] = key
                        list.append(f":domain{i}_{j}, :mechanism{i}_{j}, :value{i}_{j}, :qualifier{i}_{j}")
                elif key in ("include"):
                    for j, m in enumerate(mechanisms[key]):
                        kwargs[f"domain{i}_{j}"] = domain
                        kwargs[f"mechanism{i}_{j}"] = key
                        kwargs[f"value{i}_{j}"] = m.get('domain')
                        kwargs[f"qualifier{i}_{j}"] = m.get('qualifier')
                        list.append(f":domain{i}_{j}, :mechanism{i}_{j}, :value{i}_{j}, :qualifier{i}_{j}")
                elif key in ("redirect"):
                    if mechanisms[key]:
                        kwargs[f"domain{i}"] = domain
                        kwargs[f"mechanism{i}"] = key
                        kwargs[f"value{i}"] = mechanisms[key].get('domain')
                        kwargs[f"qualifier{i}"] = None
                        list.append(f":domain{i}, :mechanism{i}, :value{i}, :qualifier{i}")
                elif key in ("exp"):
                    if mechanisms[key]:
                        kwargs[f"domain{i}"] = domain
                        kwargs[f"mechanism{i}"] = key
                        kwargs[f"value{i}"] = mechanisms[key]
                        kwargs[f"qualifier{i}"] = None
                        list.append(f":domain{i}, :mechanism{i}, :value{i}, :qualifier{i}")
                else:
                    if mechanisms[key]:
                        kwargs[f"domain{i}"] = domain
                        kwargs[f"mechanism{i}"] = key
                        kwargs[f"value{i}"] = None
                        kwargs[f"qualifier{i}"] = mechanisms[key]
                        list.append(f":domain{i}, :mechanism{i}, :value{i}, :qualifier{i}")
                if len(list) >= self.DATABASE_INSERT_CHUNK_SIZE:
                    query = text(
                        f"INSERT {'IGNORE' if table != 'mechanisms' else ''} INTO `{table}`(`domain`, `mechanism`, `value`, `qualifier`) VALUES (" +
                        "), (".join(list) +
                        ")")
                    self.worker.execute(query, kwargs)
                    list = []
            if len(list) > 0:
                query = text(
                    f"INSERT {'IGNORE' if table != 'mechanisms' else ''} INTO `{table}`(`domain`, `mechanism`, `value`, `qualifier`) VALUES (" +
                    "), (".join(list) +
                    ")")
                self.worker.execute(query, kwargs)
        except SQLAlchemyError as e:
            print(f"Database Error: '{str(e)}'")

    def insert_asns(self, domain, asns, table="asns"):
        if len(asns) == 0:
            return
        kwargs = {}
        list = []
        for i, asn in enumerate(asns):
            kwargs[f"domain{i}"] = domain
            kwargs[f"asn{i}"] = asn
            list.append(f":domain{i}, :asn{i}")
            if len(list) >= self.DATABASE_INSERT_CHUNK_SIZE:
                query = text(f"INSERT {'IGNORE' if table != 'asns' else ''} INTO `{table}`(`domain`, `asn`) VALUES (" +
                             "), (".join(list) +
                             ")")
                self.worker.execute(query, kwargs)
                list = []
        if len(list) > 0:
            query = text(f"INSERT {'IGNORE' if table != 'asns' else ''} INTO `{table}`(`domain`, `asn`) VALUES (" +
                         "), (".join(list) +
                         ")")
            self.worker.execute(query, kwargs)

    def insert_errors(self, domain, items, items_type, table="errors"):
        if len(items) == 0:
            return
        kwargs = {}
        list = []
        for i, item in enumerate(items):
            kwargs[f"domain{i}"] = domain
            kwargs[f"mechanism{i}"] = self._get_or_none(item, "mechanism")
            kwargs[f"type{i}"] = items_type
            kwargs[f"class{i}"] = type(item).__name__
            kwargs[f"msg{i}"] = str(item)
            list.append(f":domain{i}, :mechanism{i}, :type{i}, :class{i}, :msg{i}")
            if len(list) >= self.DATABASE_INSERT_CHUNK_SIZE:
                query = text(
                    f"INSERT {'IGNORE' if table != 'errors' else ''} INTO `{table}`(`domain`, `mechanism`, `type`, `class`, `msg`) VALUES (" +
                    "), (".join(list) +
                    ")")
                self.worker.execute(query, kwargs)
                list = []
        if len(list) > 0:
            query = text(
                f"INSERT {'IGNORE' if table != 'errors' else ''} INTO `{table}`(`domain`, `mechanism`, `type`, `class`, `msg`) VALUES (" +
                "), (".join(list) +
                ")")
            self.worker.execute(query, kwargs)

    def insert_empty(self, domain, has_mx, dnssec=None):
        query = text("INSERT IGNORE INTO empty VALUES (:domain, :has_mx, :dnssec)")
        self.worker.execute(query, domain=domain, has_mx=has_mx, dnssec=dnssec)

import concurrent
import ipaddress
import os

import pandas as pd

import dns.resolver

from .checkdmarc import test_dnssec


class SPFResolver:
    TIMEOUT = 20
    RETRIES = 2
    DNSSEC_TEST_DOMAIN = 'cloudflare.com'

    def __init__(self, nameservers):
        self.resolver = dns.resolver.Resolver()
        self.resolver.nameservers = list(nameservers)
        self.resolver.rotate = True
        self.resolver.search = None
        self.resolver.retry_servfail = True

    def resolve(self, *args, **kwargs):
        tries = 0
        while True:
            try:
                return self.resolver.resolve(*args, **kwargs)
            except BaseException as e:
                tries += 1
                if tries >= self.RETRIES:
                    raise e

    @staticmethod
    def test_dnsserver(server, dns_servers, timeout=2.0):
        tmp_resolver = dns.resolver.Resolver()
        tmp_resolver.cache = False
        tmp_resolver.timeout = timeout
        tmp_resolver.lifetime = timeout
        tmp_resolver.nameservers = [server]
        try:
            tmp_resolver.resolve("google.de")
            dns_servers.add(server)
        except:
            pass

    @staticmethod
    def get_nameservers(path):
        print("Check nameservers")
        dns_servers = set()
        with concurrent.futures.ThreadPoolExecutor(max_workers=100) as executor:
            for file in os.listdir(path):
                if file.endswith(".csv"):
                    nameservers_df = pd.read_csv(os.path.join(path, file))
                    for _, item in nameservers_df.iterrows():
                        try:
                            address = ipaddress.ip_address(item.ip_address)
                        except:
                            continue
                        if item.reliability >= 0.9 and item.dnssec and isinstance(address, ipaddress.IPv4Address):
                            if test_dnssec(SPFResolver.DNSSEC_TEST_DOMAIN, nameservers=[item.ip_address]):
                                executor.submit(SPFResolver.test_dnsserver, item.ip_address, dns_servers)
        print(f"Using {len(dns_servers)} nameservers")
        return dns_servers

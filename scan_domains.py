import datetime
import os
import traceback
from collections import OrderedDict
from multiprocessing import Pool

import pyasn
from dotenv import load_dotenv
from sqlalchemy.exc import SQLAlchemyError
from tqdm import tqdm

from utils.checkdmarc import get_base_domain, get_mx_hosts, DNSException, query_spf_record, parse_spf_record, SPFError, \
    query_dmarc_record, parse_dmarc_record, DMARCError, SPFRecordNotFound, DMARCRecordNotFound, _NotImplemented, \
    test_dnssec
from utils.database import SPFDatabaseWorker
from utils.dns import SPFResolver


# Multiprocessing
def initializer(nameservers):
    global DATABASE_WORKER
    DATABASE_WORKER = SPFDatabaseWorker(DATABASE_ENGINE)
    global SPF_RESOLVER
    SPF_RESOLVER = SPFResolver(nameservers)
    global ASN_DB
    ASN_DB = pyasn.pyasn(os.getenv('ASN_DB'))


class ScanResult():
    def __init__(self, domain):
        self.domain = domain
        self.base_domain = get_base_domain(domain)
        self.mx = []
        self.spf = OrderedDict([("record", None), ("existing", True), ("valid", True), ("dns_lookups", None), ("dns_void_lookups", None),
                                ("allowed_ip4", None), ("allowed_ip6", None), ("asns", None), ("errors", [])])
        self.dmarc = OrderedDict([("record", None), ("existing", True), ("valid", True), ("location", None), ("warnings", []), ("errors", [])])
        self.dnssec = None

    @property
    def has_mx(self):
        if len(self.mx['hosts']) > 0:
            return True
        else:
            return False

    @property
    def has_spf(self):
        if self.spf['record']:
            return True
        for error in self.spf['errors']:
            if isinstance(error, SPFRecordNotFound):
                return False
        return False

    @property
    def has_dmarc(self):
        for error in self.dmarc['errors']:
            if isinstance(error, DMARCRecordNotFound):
                return False
        return True

    @property
    def incomplete(self):
        for error in self.spf['errors']:
            if isinstance(error, _NotImplemented):
                return True
        return False

    @property
    def error_count(self):
        errors = 0
        for error in self.spf["errors"]:
            if isinstance(error, SPFError):
                errors += 1
        return errors

    @property
    def include_count(self):
        try:
            return len(self.spf["parsed"]["include"])
        except KeyError:
            return None

    def _get_include_count(self, result):
        includes = 0
        for item in result["parsed"]["include"]:
            includes += 1
            includes += self._get_include_count(item)
        return includes

    @property
    def include_recursive_count(self):
        try:
            return self._get_include_count(self.spf)
        except KeyError:
            return None


    @property
    def allowed_asn_count(self):
        try:
            return len(self.spf["asns"])
        except (KeyError, TypeError):
            return None

    @property
    def allowed_asn_count_complete(self):
        try:
            if None in self.spf["asns"]:
                return False
            else:
                return True
        except (KeyError, TypeError):
            return False

    @property
    def dmarc_p(self):
        try:
            if self.dmarc['tags']['p']['value'] == 'none':
                return None
            else:
                return self.dmarc['tags']['p']['value']
        except KeyError:
            return None

    @property
    def dmarc_sp(self):
        try:
            if self.dmarc['tags']['sp']['value'] == 'none':
                return None
            else:
                return self.dmarc['tags']['sp']['value']
        except KeyError:
            return None

    @property
    def dmarc_adkim(self):
        try:
            if self.dmarc['tags']['adkim']['value'] == 'none':
                return None
            else:
                return self.dmarc['tags']['adkim']['value']
        except KeyError:
            return None

    @property
    def dmarc_aspf(self):
        try:
            if self.dmarc['tags']['aspf']['value'] == 'none':
                return None
            else:
                return self.dmarc['tags']['aspf']['value']
        except KeyError:
            return None


def process(row):
    domain = row[1].domain.lower()

    domain_results = ScanResult(domain)
    try:
        # MX
        try:
            domain_results.mx = get_mx_hosts(
                domain, skip_tls=True,
                resolver=SPF_RESOLVER.resolver,
                timeout=SPF_RESOLVER.TIMEOUT)
        except DNSException as error:
            domain_results.mx = OrderedDict([("hosts", []),
                                                ("error", error)])
        except BaseException as e:
            print(f"Unknown Error resolving '{domain}': {e}")
            return
        # SPF
        try:
            spf_query = query_spf_record(
                domain,
                resolver=SPF_RESOLVER,
                timeout=SPF_RESOLVER.TIMEOUT)
            domain_results.spf["record"] = spf_query["record"]
            domain_results.spf["warnings"] = spf_query["warnings"]
            parsed_spf = parse_spf_record(domain_results.spf["record"],
                                          domain_results.domain,
                                          asn_db=ASN_DB,
                                          resolver=SPF_RESOLVER,
                                          timeout=SPF_RESOLVER.TIMEOUT,
                                          mx=domain_results.mx,
                                          database_worker=DATABASE_WORKER)

            domain_results.spf["dns_lookups"] = parsed_spf[
                "dns_lookups"]
            domain_results.spf["dns_void_lookups"] = parsed_spf[
                "dns_void_lookups"]
            domain_results.spf["allowed_ip4"] = parsed_spf["allowed_ip4"]
            domain_results.spf["allowed_ip6"] = parsed_spf["allowed_ip6"]
            domain_results.spf["asns"] = parsed_spf["asns"]
            domain_results.spf["parsed"] = parsed_spf["parsed"]
            domain_results.spf["warnings"] += parsed_spf["warnings"]
            domain_results.spf["errors"] += parsed_spf["errors"]
        except SPFError as error:
            domain_results.spf["errors"].append(error)
        if len(domain_results.spf["errors"]) != 0:
            domain_results.spf["valid"] = False

        # DMARC
        try:
            dmarc_query = query_dmarc_record(domain,
                                             resolver=SPF_RESOLVER,
                                             timeout=SPF_RESOLVER.TIMEOUT)
            domain_results.dmarc["record"] = dmarc_query["record"]
            domain_results.dmarc["location"] = dmarc_query["location"]
            parsed_dmarc_record = parse_dmarc_record(
                dmarc_query["record"],
                dmarc_query["location"],
                resolver=SPF_RESOLVER,
                timeout=SPF_RESOLVER.TIMEOUT)
            domain_results.dmarc["warnings"] = dmarc_query["warnings"]

            domain_results.dmarc["tags"] = parsed_dmarc_record["tags"]
            domain_results.dmarc["warnings"] += parsed_dmarc_record[
                "warnings"]
        except DMARCRecordNotFound:
            domain_results.dmarc["existing"] = False
        except DMARCError as error:
            domain_results.dmarc["errors"].append(error)
        if len(domain_results.dmarc["errors"]) != 0:
            domain_results.dmarc["valid"] = False

        try:
            domain_results.dnssec = test_dnssec(domain,
                                             resolver=SPF_RESOLVER.resolver,
                                             timeout=SPF_RESOLVER.TIMEOUT)
        except BaseException as e:
            print(e)

        try:
            if domain_results.has_spf:
                DATABASE_WORKER.insert_spf(domain_results.domain, domain_results.has_mx, domain_results.spf["record"], domain_results.spf["valid"], domain_results.incomplete,
                                           domain_results.error_count, domain_results.include_count, domain_results.include_recursive_count,
                                           domain_results.spf["allowed_ip4"], domain_results.spf["allowed_ip6"],
                                           domain_results.allowed_asn_count, domain_results.allowed_asn_count_complete, domain_results.dmarc["record"],
                                           domain_results.dmarc["valid"], domain_results.dmarc_p, domain_results.dmarc_sp, domain_results.dmarc_adkim,
                                           domain_results.dmarc_aspf, domain_results.dnssec)
                if 'parsed' in domain_results.spf:
                    DATABASE_WORKER.insert_includes(domain_results.domain, domain_results.spf['parsed']['include'])
                    DATABASE_WORKER.insert_mechanisms(domain_results.domain, domain_results.spf['parsed'])
                    DATABASE_WORKER.insert_asns(domain_results.domain, domain_results.spf['asns'])
                DATABASE_WORKER.insert_errors(domain_results.domain, domain_results.spf['errors'], "error")
                DATABASE_WORKER.insert_errors(domain_results.domain, domain_results.spf['warnings'], "warning")
                DATABASE_WORKER.insert_errors(domain_results.domain, domain_results.dmarc['errors'], "error")
                DATABASE_WORKER.insert_errors(domain_results.domain, domain_results.dmarc['warnings'], "warning")
            else:
                DATABASE_WORKER.insert_empty(domain, has_mx=domain_results.has_mx, dnssec=domain_results.dnssec)
        except SQLAlchemyError as e:
            DATABASE_WORKER.delete_domain(domain)
            print(f"Database Error at '{domain}': {type(e).__name__}: {e}")
    except BaseException as e:
        print(f"Error processing '{domain}': {type(e).__name__}: {e}")
        traceback.print_exc()
        return


if __name__ == '__main__':
    load_dotenv()
    DATABASE_ENGINE = os.getenv('DATABASE_ENGINE')
    WORKERS = int(os.getenv('WORKERS', '1'))

    spf_database = SPFDatabaseWorker(DATABASE_ENGINE)

    todo_df = spf_database.get_data('domains', 'domain')
    print(f"{len(todo_df)} responses in total in database.")

    done_df = spf_database.get_data('spf', 'domain')
    print(f"{len(done_df)} are already processed.")

    empty_df = spf_database.get_data('empty', 'domain')
    print(f"{len(empty_df)} are known to be empty.")

    tasks_df = todo_df.drop(todo_df[todo_df['domain'].isin(empty_df['domain'])].index)
    tasks_df.drop(tasks_df[tasks_df['domain'].isin(done_df['domain'])].index, inplace=True)
    print(f"{len(tasks_df)} to be processed.")

    nameservers = SPFResolver.get_nameservers("assets/dns-servers")

    start_date = datetime.datetime.now()
    pool = Pool(initializer=initializer, initargs=(nameservers,), processes=WORKERS)
    result = list(tqdm(pool.imap_unordered(process, tasks_df.iterrows())))
    stop_date = datetime.datetime.now()
    print(f"Iterations per Second: {len(tasks_df) / (stop_date - start_date).seconds}")
